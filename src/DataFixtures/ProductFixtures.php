<?php

namespace App\DataFixtures;

use App\Entity\Product;
use App\Entity\ProductImage;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Generator;
use Faker\Provider\Lorem;

class ProductFixtures extends Fixture
{
    protected const PRODUCT_COUNT = 50000;

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('en_US');
        for ($i = 0; $i < self::PRODUCT_COUNT; $i++) {
            $product = new Product();
            $product->setStyleNumber($faker->unique()->lexify('???|???'));
            $product->setName($faker->words(3, true));
            $product->setPrice($faker->numberBetween(0, 1000000) / 100.0);
            $product->setCurrency("USD");
            for ($j = 0; $j < random_int(0, 10); $j++) {
                $image = new ProductImage($faker->url);
                $image->setProduct($product);
                $product->getImages()->add($image);
            }
            $manager->persist($product);
        }

        $manager->flush();
    }
}
