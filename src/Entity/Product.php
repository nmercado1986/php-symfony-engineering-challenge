<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;
use Symfony\Component\Validator\Constraints as Assert;
use App\Entity\ProductImage;

/**
 * @ORM\Entity(repositoryClass=ProductRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class Product
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $styleNumber;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     * @Assert\Type(type="integer")
     */
    private $price;

    /**
     * @ORM\Column(type="string", length=3)
     * @Assert\EqualTo("USD")
     */
    private $currency;

    /**
     * @ORM\Column(type="datetime",nullable=true)
     */
    private $lastPublishedAt;

    /**
     * @ORM\Column(type="datetime",nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\OneToMany(targetEntity=ProductImage::class,mappedBy="product",cascade="persist")
     * @ORM\JoinColumn(name="product_id")
     */
    private $images;


    public function __construct()
    {
        $this->images = new ArrayCollection([]);
    }

    public function getStyleNumber(): ?string
    {
        return $this->styleNumber;
    }

    public function setStyleNumber(string $styleNumber): self
    {
        $this->styleNumber = $styleNumber;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price / 100.0;
    }

    public function setPrice(float $price): self
    {
        $this->price = round(100 * $price);

        return $this;
    }

    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    public function setCurrency(string $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Updates the last updated date if some data has changed in the product
     * Then we can query when the last updated date > last published date and determine if needs sync
     *
     * @ORM\PrePersist()
     * @return void
     */
    public function setUpdatedAt($args): void
    {
        $this->updatedAt = new \DateTime();
    }

    /**
     * @return ArrayCollection
     */
    public function getImages() : Collection
    {
        return $this->images;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @param mixed $lastPublishedAt
     */
    public function setLastPublishedAt($lastPublishedAt): void
    {
        $this->lastPublishedAt = $lastPublishedAt;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @return mixed
     */
    public function getLastPublishedAt()
    {
        return $this->lastPublishedAt;
    }

}
