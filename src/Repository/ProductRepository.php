<?php

namespace App\Repository;

use App\Entity\Product;
use App\Entity\ProductImage;
use App\Utils\ProductExporterInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManager;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Persistence\ManagerRegistry;
use Iterator;
use JsonException;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{

    protected const BATCH_IMPORT_SIZE = 1000;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    /**
     * @param string $json
     * @return void
     * @throws JsonException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function importJson(string $json): void
    {
        $productData = json_decode($json, true, 512, JSON_THROW_ON_ERROR);
        $imported = 0;
        foreach ($productData as $item) {
            $product = $this->findOneBy([
                'styleNumber' => $item['styleNumber']
            ]);
            if ($product === null) {
                $product = new Product;
            }
            $product->setStyleNumber($item['styleNumber']);
            $product->setName($item['name']);
            $product->setPrice($item['price']['amount']);
            $product->setCurrency($item['price']['currency']);
            $this->_em->persist($product);

            while ($currentImg = $product->getImages()->next()) {
                $this->_em->remove($currentImg);
            }
            foreach($item['images'] as $anImage) {
                $image = new ProductImage($anImage);
                $image->setProduct($product);
                $product->getImages()->add($image);
            }

            if ($imported + 1 === self::BATCH_IMPORT_SIZE) {
                $imported = 0;
                $this->_em->flush();
            }
        }
        $this->_em->flush();
    }

    /**
     * Finds any products that were never published or that haven't been published since their last update.
     * Update is tracked using Doctrine lifecycle events whenever a product is updated.
     * See Product::setUpdatedAt()
     *
     * @return Iterator The generator to iterate over products
     */
    public function findProductsToSync() : Iterator {
        $query = $this->_em->createQuery("SELECT p FROM App\Entity\Product p WHERE p.lastPublishedAt < p.updatedAt OR p.lastPublishedAt IS NULL");

        foreach ($query->iterate() as $product) {
            yield $product[0];
        }

    }

}
