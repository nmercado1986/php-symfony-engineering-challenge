<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210320220505 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Adds column for controlling product last published date';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $schema->getTable('Product')->addColumn('last_published_at', "datetime");
        $schema->getTable('Product')->addColumn('updated_at', "datetime");

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $schema->getTable('Product')->dropColumn('last_published_at');
        $schema->getTable('Product')->dropColumn('updated_at');

    }
}
