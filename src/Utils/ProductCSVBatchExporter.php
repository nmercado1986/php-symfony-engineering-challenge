<?php


namespace App\Utils;


use DateTime;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Iterator;
use phpDocumentor\Reflection\Types\Resource_;
use RuntimeException;

class ProductCSVBatchExporter implements ProductExporterInterface
{

    private const BATCH_SIZE = 5000;

    private array $currentBatch = [];
    private int $currentBatchMaxImageCount = 0;

    private $latestFile = null;

    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * The method receives an Iterable which is expected to contain products.
     * It creates a fresh file every BATCH_SIZE records.
     * Each file has this process PID in the name to avoid collision with other workers.
     * Also, each file has a separate timestamp.
     *
     * @param Iterator $products Set of products to iterate over and export
     *
     * @return int The count of products synced
     */
    public function exportBatch(Iterator $products) : int
    {
        $i = 0;

        foreach ($products as $aProduct) {
            /**
             * The CSV in the example specified a possibly unrestricted number of columns for images.
             * So for each batch, find the row with the most images and record it, so we know how many columns the CSV needs
             */
            $imageCount = count($aProduct->getImages());

            if ($imageCount > $this->currentBatchMaxImageCount) {
                $this->currentBatchMaxImageCount = $imageCount;
            }

            $productRow = [
                $aProduct->getStyleNumber(),
                $aProduct->getName(),
                "$" . $aProduct->getPrice()
            ];
            $imageColumns = [];
            foreach ($aProduct->getImages() as $image) {
                $imageColumns[] = $image->getUrl();
            }
            $this->currentBatch []= array_merge($productRow, $imageColumns);

            $aProduct->setLastPublishedAt(new DateTime());
            $this->entityManager->persist($aProduct);


            if ($i === self::BATCH_SIZE) {
                $this->flushCurrentBatch();
                $this->entityManager->flush();
                $fp = $this->createNewFile();
                $i = 0;
            }
            $i++;
        }
        $this->flushCurrentBatch();
        $this->entityManager->flush();

        return $i;

        // TODO: Transfer it over sftp to the destination server

    }

    /**
     * @return false|object|resource
     */
    protected function createNewFile()
    {
        /**
         * Let's create a separate csv file per worker so that
         * we can run multiple processes each serving a separate batch
         * As PIDs are not guaranteed to be unique, let's also add the timestamp
         */
        $pid = getmypid();
        $timestamp = (new DateTime)->format('Y-m-d-H-i-s-u');
        $filepath = "product-export-{$timestamp}-{$pid}.csv";
        $fp = fopen($filepath, "cb");

        if (!$fp) {
            throw new RuntimeException('Could not create new batch file. Aborting');
        }

        $this->latestFile = $filepath;


        return $fp;
    }

    /**
     * Returns the path of the last file, for testing purposes
     *
     * @return string
     */
    public function getLatestFile(): ?string
    {
        return $this->latestFile;
    }

    /**
     * Prepares a new CSV file for the current batch
     * Then flushes the contents of the current batch into the file
     * This appends the header line based on calculating how many image column the batch needs
     */
    protected function flushCurrentBatch(): void
    {
        if (count($this->currentBatch) === 0) {
            return;
        }

        $fp = $this->createNewFile();

        //
        $header = [
            "Product Id",
            "Product Name",
            "Price",
        ];

        for ($i = 1; $i <= $this->currentBatchMaxImageCount; $i++) {
            $header []= "Image $i";
        }

        fputcsv($fp, $header, ',', '"');

        foreach ($this->currentBatch as $row) {
            fputcsv($fp, $row, ',', '"');
        }
        $this->currentBatch = [];
        $this->currentBatchMaxImageCount = 0;

        fflush($fp);
        fclose($fp);
    }
}