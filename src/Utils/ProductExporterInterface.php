<?php

namespace App\Utils;

use App\Entity\Product;
use Iterator;

/**
 * Interface ProductExporterInterface
 * @author nmercado
 */
interface ProductExporterInterface
{
    /**
     *
     * Iterates over products and exports them in some way, then returns the count
     *
     * @param $products Iterator
     *
     * @return int
     */
    public function exportBatch(Iterator $products) : int;
}
