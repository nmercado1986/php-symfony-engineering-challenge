<?php

namespace App\Command;

use App\Repository\ProductRepository;
use App\Utils\ProductExporterInterface;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\TransactionRequiredException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ProductExportCommand extends Command
{
    protected static $defaultName = 'product:export';
    protected static $defaultDescription = 'Exports products updated since they were last published';
    private $repository;
    private $exporter;

    public function __construct(ProductRepository $repository, ProductExporterInterface $exporter)
    {
        $this->repository = $repository;
        $this->exporter = $exporter;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription(self::$defaultDescription)
            ->addArgument('limit', InputArgument::OPTIONAL, 'Max amount of products to sync.');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $limit = $input->getArgument('limit');

        try {
            $products = $this->repository->findProductsToSync($limit);
            $result = $this->exporter->exportBatch($products);
            $io->success("{$result} products synced successfully");
            return Command::SUCCESS;
        } catch (ORMException $e) {
            $io->error("Problem retrieving products to sync: {$e->getMessage()}");
            return Command::FAILURE;
        }

    }
}
