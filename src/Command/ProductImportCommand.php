<?php

namespace App\Command;

use App\Repository\ProductRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Exception;
use Psr\Log\LoggerInterface;


use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ProductImportCommand extends Command
{
    protected static $defaultName = 'product:import';
    protected static $defaultDescription = 'Imports a json file of products';
    /**
     * @var ProductRepository
     */
    private ProductRepository $repository;

    /**
     * @param LoggerInterface $logger
     */
    public function __construct(ProductRepository $repository)
    {
        $this->repository = $repository;
        parent::__construct();
    }
    

    protected function configure()
    {
        $this
            ->setDescription(self::$defaultDescription)
            ->addArgument('jsonFile', InputArgument::REQUIRED, 'The JSON file to import')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $jsonFile = $input->getArgument('jsonFile');

        try {
            $this->repository->importJson(file_get_contents($jsonFile));
        } catch (OptimisticLockException $e) {
            $io->error("Problem acquiring DB lock");
        } catch (ORMException $e) {
            $io->error("Problem writing imported entities");
        } catch (\JsonException $e) {
            $io->error("Invalid JSON");
        }

        if ($jsonFile) {
            $io->note(sprintf('Importing products from %s', $jsonFile));
        }

        return Command::SUCCESS;
    }
}
