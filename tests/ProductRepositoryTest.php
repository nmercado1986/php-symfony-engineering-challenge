<?php

namespace App\Tests;

use App\Repository\ProductRepository;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ProductRepositoryTest extends KernelTestCase
{
    public function testCanImportJson(): void
    {
        $kernel = self::bootKernel();
        $repo = self::$container->get(ProductRepository::class);

        $productJson = '[{"styleNumber": "ABC|123", "name": "T-Shirt", "price": {"amount": 1500, "currency": "USD"}, "images": ["https://via.placeholder.com/400x300/4b0082?id=1", "https://via.placeholder.com/400x300/4b0082?id=2"]}]';

        $repo->importJson($productJson);

        $products = $repo->findAll();

        $this->assertCount(1, $products);
        $this->assertEquals('ABC|123', $products[0]->getStyleNumber());
        $this->assertEquals('T-Shirt', $products[0]->getName());
        $this->assertEquals(1500, $products[0]->getPrice());
        $this->assertEquals("USD", $products[0]->getCurrency());
        $this->assertEquals("https://via.placeholder.com/400x300/4b0082?id=1", $products[0]->getImages()->get(0)->getUrl());
        $this->assertEquals("https://via.placeholder.com/400x300/4b0082?id=2", $products[0]->getImages()->get(1)->getUrl());
        $this->assertNotNull($products[0]->getUpdatedAt());
        $this->assertNull($products[0]->getLastPublishedAt());
    }


    /**
     * @depends testCanImportJson
     */
    public function testDetectsUnpublishedProducts() : void
    {
        self::bootKernel();
        $repo = self::$container->get(ProductRepository::class);

        $count = 0;
        foreach ($repo->findProductsToSync() as $product) {
            $count++;
        }

        $this->assertEquals(1, $count);

    }
}
