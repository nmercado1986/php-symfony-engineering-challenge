<?php

namespace App\Tests;

use App\Entity\Product;
use App\Entity\ProductImage;
use ArrayIterator;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use App\Utils\ProductCSVBatchExporter;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ProductFileExporterTest extends KernelTestCase
{
    private $exporter;

    public function testCanExportProduct(): void
    {
        self::bootKernel();
        $exporter = new ProductCSVBatchExporter(self::$container->get(EntityManagerInterface::class));

        $product = new Product;
        $product->setStyleNumber('ABC|123');
        $product->setName('Macbook Pro 2013');
        $product->setPrice(1300);
        $product->setCurrency('USD');
        $product->getImages()->add(new ProductImage('https://google.com/'));
        $product->getImages()->add(new ProductImage('https://apple.com/'));

        $exporter->exportBatch(new ArrayIterator([$product]));
        $filepath = $exporter->getLatestFile();

        $this->assertFileExists($filepath);

        $fp = fopen($filepath, "r");
        $header = fgets($fp);
        $this->assertEquals("\"Product Id\",\"Product Name\",Price,\"Image 1\",\"Image 2\"\n", $header);
        $firstRow = fgets($fp);
        $this->assertEquals("ABC|123,\"Macbook Pro 2013\",$1300,https://google.com/,https://apple.com/\n", $firstRow);
    }
}
